/*
 * grunt-wpt2db
 * 
 *
 * Copyright (c) 2016 Marc Sureda
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    wpt2db: {
      default_options: {
        options: {
        }
      },
      custom_options: {
        options: {
          instanceUrl: 'www.webpagetest.org',
          wptApiKey: null,
          testUrl: 'http://www.google.com',
          runs: 1,
          location: '',
          notifydocumentdb: true,
          documentdbmasterKey: '',
          documentdbhost: '',
          documentdbdatabaseDefinition: '',
          documentdbcollectionDefinition: '',
          documentdbdocumentDefinitionId: '',
          notifyLogstash: false,
          logstashHost: 'localhost',
          logstashPort: null
        }
      }
    },

    // Unit tests.
    nodeunit: {
      tests: ['test/*_test.js']
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['clean', 'wpt2db', 'nodeunit']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'test']);

};
