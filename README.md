# grunt-wpt2db

> Plugin used to add information from wpt to document-db or Logstash from some origins, via JSON data

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-wpt2db --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-wpt2db');
```

## The "wpt2db" task

### Overview
In your project's Gruntfile, add a section named `wpt2db` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  wpt2db: {
    options: {
        instanceUrl: 'www.webpagetest.org',
        wptApiKey: {YOUR WEBPAGETEST API KEY},
        testUrl: 'http://www.google.com',
        runs: 1,
        location: 'Dulles:Chrome',
        notifydocumentdb: true,
        documentdbmasterKey: '{KEY FROM DOCUMENTDB}',
        documentdbhost: '{HOST OF DOCUMENTDB}',
        documentdbdatabaseDefinition: 'TEST',
        documentdbcollectionDefinition: 'TEST',
        documentdbdocumentDefinitionId: 'TEST',
        notifyLogstash: false,
        logstashHost: 'localhost',
        logstashPort: null
    }
  },
});
```

### Options

#### options.instanceUrl
Type: `String`
Default value: `'www.webpagetest.org'`

A string value that is used to do configure the webpagetest instance.

#### options.wptApiKey
Type: `String`
Default value: `'NULL'`

A string value that is used to do configure the api key needed to connect to an instance of webpagetest


#### options.runs
Type: `int`
Default value: `1`

A int value that is used to do configure number of calls to webpagetest.

#### options.location
Type: `String`
Default value: `''`

A string value that is used to do configure the location used in webpagetest.

#### options.notifydocumentdb
Type: `boolean`
Default value: `true`

A boolean value that is used to do configure if you want to send the data to document db.

#### options.documentdbmasterKey
Type: `String`
Default value: `''`

A string value that is used to do configure master key of document db.

#### options.documentdbhost
Type: `String`
Default value: `''`

A string value that is used to do configure the url of the document db server.

#### options.documentdbdatabaseDefinition
Type: `String`
Default value: `''`

A string value that is used to do configure name of the database.

#### options.documentdbcollectionDefinition
Type: `String`
Default value: `''`

A string value that is used to do configure the name of the collection.

#### options.documentdbdocumentDefinitionId
Type: `String`
Default value: `''`

A string value that is used to do configure the name of the document.

#### options.notifyLogstash
Type: `boolean`
Default value: `false`

A boolean value that is used to do configure if you want to send the data to logstash (It is needed FileBeat configurated to use this option).

#### options.logstashHost
Type: `String`
Default value: `'localhost'`

A string value that is used to do configure the host of the logstash.

#### options.logstashPort
Type: `int`
Default value: `Null`

A string value that is used to do configure the port of the logstash.


### Logstash part is still unestable

### Usage Examples

```js
grunt.initConfig({
  wpt2db: {
    options: {
            instanceUrl: 'www.webpagetest.org',
            wptApiKey: {YOUR WEBPAGETEST API KEY},
            testUrl: 'http://www.google.com',
            runs: 1,
            location: 'Dulles:Chrome',
            notifydocumentdb: true,
            documentdbmasterKey: '{KEY FROM DOCUMENTDB}',
            documentdbhost: '{HOST OF DOCUMENTDB}',
            documentdbdatabaseDefinition: 'TEST',
            documentdbcollectionDefinition: 'TEST',
            documentdbdocumentDefinitionId: 'TEST',
            notifyLogstash: false,
            logstashHost: 'localhost',
            logstashPort: null
        }
  },
});

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
First stable version 0.1.7