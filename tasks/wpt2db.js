/*
 * grunt-wpt2db
 * 
 *
 * Copyright (c) 2016 Marc Sureda
 * Licensed under the MIT license.
 */

'use strict';
var DocumentClient = require('documentdb').DocumentClient;
var webpagetest = require('webpagetest');
var format = require('string-format');
var async = require('async');

module.exports = function(grunt) {

  var checkTestStatus = function(wpt, testId, options, done){
    wpt.getTestStatus(testId, function(err, data) {

      if (err) {
        return done(err);
      }

      grunt.log.ok("Status for " + testId + ": " + data.data.statusText);

      if (!data.data.completeTime) {
        setTimeout(function(){
          checkTestStatus(wpt, testId, options, done);
        }, 50000);
      }
      else {
        return wpt.getTestResults(testId, function(err, data) {
          grunt.log.ok(options.instanceUrl + "/result/" + testId + "/");

          if (err > 0) {
            return done(err);
          }

          var message = format('WPT results: <a href="{0}">{0}</a><br />Page under test: {1}<br /> Load Time: {2} <br />TTFB: {3}',data.data.summary, options.testUrl, data.data.median.firstView.loadTime, data.data.median.firstView.TTFB);
          grunt.log.ok(message);

          delete data.data.runs;

          async.series([
                function(callback) {
                  if (options.notifyLogstash || options.notifydocumentdb) {
                    grunt.log.ok("Notify to DB");
                    execute(this, data, options, callback);
                  }
                  else {
                    callback();
                  }
                }
              ],
              function(err, data) {
                done();
              });
        });
      }
    });
  };


  var executeDocumentDB = function(task, data, options, done){

    var documentdbdocumentDefinitionContent = data;
    var masterKey = options.documentdbmasterKey;
    var client = new DocumentClient(options.documentdbhost, {masterKey: masterKey});

    var documentIdentifier = options.documentdbdocumentDefinitionId + grunt.template.today('yyyy-mm-dd-HH-MM');
    var documentDefinition = { id: documentIdentifier, content: documentdbdocumentDefinitionContent };

    getOrCreateDatabase(client, options.documentdbdatabaseDefinition, function(err, database) {
      if(err) return console.log(err);
      getOrCreateCollection(client, database._self, options.documentdbcollectionDefinition, function(err, collection) {
        if(err) return console.log(err);
        client.createDocument(collection._self, documentDefinition, function(err, document) {
          if(err) return console.log(err);
          console.log('Created Document with content: ', document.content);
        });
      });
    });

    function cleanup(client, database) {
      client.deleteDatabase(database._self, function(err) {
        if(err) console.log(err);
      })
    }
  };

  function getOrCreateDatabase (client, databaseId, callback) {
    var querySpec = {
      query: 'SELECT * FROM root r WHERE r.id= @id',
      parameters: [{
        name: '@id',
        value: databaseId
      }]
    };

    client.queryDatabases(querySpec).toArray(function (err, results) {
      if (err) {
        callback(err);

      } else {
        if (results.length === 0) {
          var databaseSpec = {
            id: databaseId
          };

          client.createDatabase(databaseSpec, function (err, created) {
            callback(null, created);
          });

        } else {
          callback(null, results[0]);
        }
      }
    });
  };

  function getOrCreateCollection (client, databaseLink, collectionId, callback) {
    var querySpec = {
      query: 'SELECT * FROM root r WHERE r.id=@id',
      parameters: [{
        name: '@id',
        value: collectionId
      }]
    };

    client.queryCollections(databaseLink, querySpec).toArray(function (err, results) {
      if (err) {
        callback(err);

      } else {
        if (results.length === 0) {
          var collectionSpec = {
            id: collectionId
          };

          var requestOptions = {
            offerType: 'S1'
          };

          client.createCollection(databaseLink, collectionSpec, requestOptions, function (err, created) {
            callback(null, created);
          });

        } else {
          callback(null, results[0]);
        }
      }
    });
  };

  var executeLogstash = function(task, data, options, done){
    data.data.median.firstView.requests = [];
    data.data.median.repeatView.requests = [];
    data.data.median.firstView.domains = [];
    data.data.median.repeatView.domains = [];

    //MEDIAN
    var list = [];
    for (var key in data.data.median.firstView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.median.firstView[list[i]];
    }
    var list = [];
    for (var key in data.data.median.repeatView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.median.repeatView[list[i]];
    }

    //AVERAGE
    var list = [];
    for (var key in data.data.average.firstView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.average.firstView[list[i]];
    }
    var list = [];
    for (var key in data.data.average.repeatView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.average.repeatView[list[i]];
    }

    //standardDeviation
    var list = [];
    for (var key in data.data.standardDeviation.firstView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.standardDeviation.firstView[list[i]];
    }
    var list = [];
    for (var key in data.data.standardDeviation.repeatView) {
      if (key.indexOf("CSI") != -1) {
        list.push(key);
      }
    }
    for (i = 0; i < list.length; i++) {
      delete data.data.standardDeviation.repeatView[list[i]];
    }

    var wpt = JSON.stringify(data);
    console.log(wpt);

    var output = 'log/wpt' + grunt.template.today('yyyy-mm-dd-HH-MM') + '.log';
    grunt.log.ok('Writing file: ' + output);
    grunt.file.write(output, wpt + "/n");
  };

  var execute = function(task, data, options, done){
    if (options.notifydocumentdb){
      executeDocumentDB(task,data,options, done);
    }

    if(options.notifyLogstash){
      executeLogstash(task,data,options, done);
    }
  };

  var makeRequest = function(task, done){
    var options = task.options({
      instanceUrl: 'www.webpagetest.org',
      wptApiKey: null,
      testUrl: 'http://www.google.com',
      runs: 1,
      location: '',
      notifydocumentdb: true,
      documentdbmasterKey: '',
      documentdbhost: '',
      documentdbdatabaseDefinition: '',
      documentdbcollectionDefinition: '',
      documentdbdocumentDefinitionId: '',
      notifyLogstash: false,
      logstashHost: 'localhost',
      logstashPort: null
    });
    
    var parameters = {
      runs: options.runs,
      location: options.location
    };

    var wpt = new webpagetest(options.instanceUrl, options.wptApiKey);
    var testId;

    wpt.runTest(options.testUrl, parameters, function(err, data) {
      if (data.statusCode === 200) {
        testId = data.data.testId;
        checkTestStatus(wpt, testId, options, done);
      }
    });
  };

  grunt.registerMultiTask('wpt2db', 'Plugin used to add information from wpt to document-db or Logstash from some origins, via JSON data', function() {
    var done = this.async();
    makeRequest(this, done);
  });

};
